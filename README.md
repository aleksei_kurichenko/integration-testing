# Prerequisite

* NodeJS ~14.15.1
* npm ~6.14.8

Should be compatible with older versions, but created and tested with these ones.

# Installation

```
npm i
cp .env.example .env
```

Fill up `.env` with appropriate values.

# Run

```
npm run test
```
