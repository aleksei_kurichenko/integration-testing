require('dotenv').config()
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
chai.use(chaiHttp)

describe('Invite student to classroom', () => {
    const baseUrl = process.env.BASE_URL
    const apiKey = process.env.API_KEY
    const path = '/api/v1/classroom_students'
    const ts = Math.ceil(new Date().getTime() / 1000)
    const validBody = {
        first_name: `F${ts}`,
        last_name: `L${ts}`,
        email: `e${ts}@demo${ts}.com`,
        classroom_id: process.env.CLASSROOM_ID,
    }

    it('unauthorized request should return 401', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .end((err, res) => {
                expect(res).to.have.status(401)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('api key not accepted in body', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .send({ api_key: apiKey })
            .end((err, res) => {
                expect(res).to.have.status(401)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('post body is required', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .end((err, res) => {
                expect(res).to.have.status(422)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('first name is required', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send({
                ...validBody,
                first_name: '',
            })
            .end((err, res) => {
                expect(res).to.have.status(422)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('last name is required', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send({
                ...validBody,
                last_name: '',
            })
            .end((err, res) => {
                expect(res).to.have.status(422)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('email is required', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send({
                ...validBody,
                email: '',
            })
            .end((err, res) => {
                expect(res).to.have.status(422)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('classroom_id is required', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send({
                ...validBody,
                classroom_id: '',
            })
            .end((err, res) => {
                expect(res).to.have.status(422)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('wrong classroom_id is forbidden', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send({
                ...validBody,
                classroom_id: 'wrong',
            })
            .end((err, res) => {
                expect(res).to.have.status(403)
                expect(res.body).to.be.an('object')
                done()
            })
    })

    it('valid body is actually working', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send(validBody)
            .end((err, res) => {
                expect(res).to.have.status(201)
                expect(res.body).to.be.an('object')
                expect(res.body).to.have.property('id')
                done()
            })
    })

    it('same user can\'t be created twice', (done) => {
        chai.request(baseUrl)
            .post(path)
            .set('content-type', 'application/json')
            .set('x-api-key', apiKey)
            .send(validBody)
            .end((err, res) => {
                console.log('res.body', res.body)
                expect(res).to.have.status(422)
                expect(res.body).to.be.an('object')
                done()
            })
    })
})
